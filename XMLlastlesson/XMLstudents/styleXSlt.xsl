<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Group f22</h2>

                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="group">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Last name</th>
                <th>First name</th>
                <th>PHP</th>
                <th>English</th>
                <th>Math</th>
                <th>ITE</th>
                <th>C</th>
                <td>overal_mack</td>
            </tr>
            <xsl:apply-templates select="student"/>
        </table>
    </xsl:template>


    <xsl:template match="student">
        <tr>
            <td><xsl:apply-templates select="last_name"/></td>
            <td><xsl:value-of select="first_name"/></td>
            <xsl:apply-templates select="list_subjects"/>
        </tr>
    </xsl:template>


    <xsl:template match="list_subjects">
        <td><xsl:value-of select="PHP"/></td>
        <td><xsl:value-of select="english"/></td>
        <td><xsl:value-of select="math"/></td>
        <td><xsl:value-of select="ITE"/></td>
        <td><xsl:value-of select="C"/></td>
        <td><xsl:value-of select="overal_mack"/></td>
    </xsl:template>

    <xsl:template match="last_name">
        <xsl:for-each select="group/student">
            <xsl:sort select="last_name"/>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>

<!--Два режима в одном отображает таблицу с
сортировкой по ФИО, второй по средней оценке /
сортировку по предмету которий переданный в качестве параметра-->