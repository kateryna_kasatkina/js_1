<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>My Group</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>fio</th>
                        <th>mathematics</th>
                        <th>physics</th>
                        <th>chemistry</th>
                    </tr>
                    <xsl:for-each select="group/student">
                        <xsl:sort select="fio"  />
                        <tr>
                            <td><xsl:value-of select="fio"/></td>
                            <td><xsl:value-of select="rating_mathematics"/></td>
                            <td><xsl:value-of select="rating_physics"/></td>
                            <td><xsl:value-of select="rating_chemistry"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
                <h2>average mark</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>fio</th>
                        <th>average mark</th>
                    </tr>
                    <xsl:for-each select="group/student">
                        <xsl:sort select="(rating_mathematics+rating_physics+rating_chemistry) div 3"
                                  order="descending"
                                  data-type="number"/>
                        <tr>
                            <td><xsl:value-of select="fio"/></td>
                            <td><xsl:value-of select="(rating_mathematics+rating_physics+rating_chemistry) div 3"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>