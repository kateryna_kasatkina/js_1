<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <html>
    <body>
    <!--
        This is an XSLT template file. Fill in this area with the
        XSL elements which will transform your XML to XHTML.
    -->
      Какая-то гадость<br />
	  <hr />
      <xsl:apply-templates select="//book" mode="mode1" />
      <br />
      <xsl:apply-templates select="//book" mode="mode2" />

    </body>
    </html>
</xsl:template>

  <xsl:template match="book" mode="mode1">
    <b>
      <xsl:if test="position()=2">
        <xsl:value-of select="."/>
      </xsl:if>
    </b>
  </xsl:template>

  <xsl:template match="book" mode="mode2">
    
    <xsl:if test="position()=4">
      <xsl:value-of select="name"/>|
      <xsl:value-of select="data"/>
	  <br />
    </xsl:if>
    

  </xsl:template>


</xsl:stylesheet>

