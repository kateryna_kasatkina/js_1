﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    
        <html>
          <head>
            <title>XSLT Test</title>
          </head>  
          <body>
            <xsl:apply-templates />
			
          </body>
        </html>
 </xsl:template>

  <xsl:template match="stock">
    <div style="font-weight:bold">
      ID: <xsl:apply-templates select="name"/>
      Symbol: <xsl:value-of select="symbol"/>,
      Price: <xsl:value-of select="price" />
    </div>
  </xsl:template>

  <xsl:template match="name">
    <xsl:value-of select="@id" />
    
  </xsl:template>

</xsl:stylesheet>

