var currentNewsId = 0;
var newsMaxId = 4;

$(function(){
	$("#prevBtn").click(function(){
		$("#preloader").show();
		currentNewsId--;
		if(currentNewsId < 0){
			currentNewsId = newsMaxId;
		}
		console.log(currentNewsId);
		$("#message").load("ajax.php",{ "newsid":currentNewsId},
			function(){
				$("#preloader").hide();
			});
	});
	$("#nextBtn").click(function() {
        $("#preloader").show();
        currentNewsId++;
        if (currentNewsId > newsMaxId) {
            currentNewsId = 0;
        }
        console.log(currentNewsId);
        $.get("ajax.php", {"newsid": currentNewsId+13   },
            function (responseText, textStatus, jqXHR) {
                $("#preloader").hide();

                $("#message").html(responseText.message);
                var ul = $("<ul>");
                $(responseText.errors).each(function (index,value) {
                   $("<li>").html(value).appendTo(ul);
                });
                $("#errors").append(ul);
            }, "json")
        .fail(function (responseText, textStatus) {
            console.log(responseText);
            console.log(textStatus);
            $("#preloader").hide();
        });


    });

	//$("#message").load("ajax.php",{ "newsid":currentNewsId});
});