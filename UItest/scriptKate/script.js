$(function () {
    $('#accordion').accordion({header:'h3'});
    $('#dialog').dialog({autoOpen:false});
    $('#openDialog').click(function(){
        $('#dialog').dialog('open');

    })

    $('#progress').progressbar({value:60});
    $('#slider').slider();
})