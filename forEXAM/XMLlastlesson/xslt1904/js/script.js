/**
 * Created by Саша on 19.04.2017.
 */
function loadXMLDoc(filename)
{
    if (window.ActiveXObject)//Проверка для ie
    {
       var xhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    else
    {
        xhttp = new XMLHttpRequest();//для остальных браузеров
    }
    xhttp.open("GET", filename, false);
    try {xhttp.responseType = "msxml-document"} catch(err) {} // Helping IE11
    xhttp.send("");
    return xhttp.responseXML;
}

function displayResult()
{
    var xml = loadXMLDoc("group.xml");
    var xsl = loadXMLDoc("xs.xsl");
// code for IE
//     if (window.ActiveXObject || xhttp.responseType == "msxml-document")
//     {
//         var ex = xml.transformNode(xsl);
//         document.getElementById("example").innerHTML = ex;
//     }
// code for Chrome, Firefox, Opera, etc.
    if (document.implementation && document.implementation.createDocument)
    {
        var xsltProcessor = new XSLTProcessor();//объект, который позволяет применить свойста из XSL in XML
        xsltProcessor.importStylesheet(xsl);
        var resultDocument = xsltProcessor.transformToFragment(xml, document);
        document.body.appendChild(resultDocument);
    }
}
displayResult();