<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Group f22</h2>
                <xsl:apply-templates select="group" mode="nam"/>
                <xsl:apply-templates select="group" mode="mark"/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="group" mode="nam">
        <table border="1" cellspacing="0" cellpadding="2">
            <tr bgcolor="blue">
                <th>Last name</th>
                <th>First name</th>
                <th>PHP</th>
                <th>English</th>
                <th>Math</th>
                <th>ITE</th>
                <th>C</th>
                <td>overal_mack</td>
            </tr>

            <xsl:apply-templates>
                <xsl:sort select="last_name" />
            </xsl:apply-templates>
        </table>
    </xsl:template>

    <xsl:template match="group" mode="mark">
        <table border="1" cellspacing="0" cellpadding="2">
            <tr bgcolor="blue">
                <th>Last name</th>
                <th>First name</th>
                <th>PHP</th>
                <th>English</th>
                <th>Math</th>
                <th>ITE</th>
                <th>C</th>
                <td>overal_mack</td>
            </tr>

            <xsl:apply-templates>
                <xsl:sort select="overal_mack" data-type="number" order="descending"/>
            </xsl:apply-templates>
        </table>
    </xsl:template>



    <xsl:template match="/group/student">
            <tr>
                <td><xsl:value-of select="last_name"/></td>
                <td><xsl:value-of select="first_name"/></td>
                <td><xsl:value-of select="list_subjects/PHP"/></td>
                <td><xsl:value-of select="list_subjects/english"/></td>
                <td><xsl:value-of select="list_subjects/math"/></td>
                <td><xsl:value-of select="list_subjects/ITE"/></td>
                <td><xsl:value-of select="list_subjects/C"/></td>
                <td><xsl:value-of select="list_subjects/overal_mack"/></td>
            </tr>
    </xsl:template>



</xsl:stylesheet>

<!--Два режима в одном отображает таблицу с
сортировкой по ФИО, второй по средней оценке /
сортировку по предмету которий переданный в качестве параметра-->