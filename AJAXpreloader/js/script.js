var currentNewsID=0;
var newsMaxId=4;
$(function () {
    $("#prevBtn").click(function () {
        $('#preloader').show();
        currentNewsID--;
        if(currentNewsID<0){
            currentNewsID=newsMaxId;
        }
        $('#message').load('ajax.php?newsid='+currentNewsID,
        function(){
            $('#preloader').hide();
        });
    });

    $('#nextBtn').click(function () {
        $('#preloader').show();
        currentNewsID++;
        if(currentNewsID>newsMaxId){
            currentNewsID=0;
        }
        $('#message').load('ajax.php?newsid='+currentNewsID,
            function(){
            $('#preloader').hide();
        });
    });
$('#message').load('ajax.php?newsid='+currentNewsID);
});