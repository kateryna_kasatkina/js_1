/*В произвольном месте на экране появляется DIV размером 200Х200. Через 800мс DIV пропадает. И появляется
 следующий в новых координата. Если на Divе успели нажать мышкой, то время существования следующий div
 уменьшается на 100 мс. И т.д. время постоянно уменьшается на 100 мс. Время игры следует ограничить.
 Необходимо подсчитывать кол-во попаданий.*/

//get div
var elem=document.getElementById('d');
//get span for counting points
var spanPoints=document.querySelector('span');
//var for points
var counter=1;
//var for delay
var t=800;
//adding click event for div
elem.addEventListener('mousedown',function()
{
    spanPoints.innerHTML=counter;
    counter++;
    t-=100;//reducing time
});

//get random coordinates
var x;
var y;
function get_random_coord()
{
    x=0-0.5+Math.random()*(460-0+1);
    x=Math.round(x);

    y=0-0.5+Math.random()*(380-0+1);
    y=Math.round(y);
};

//move div by random coordinates
function move_div()
{
    get_random_coord();
    elem.style.left = x+"px";
    elem.style.top = y+"px";

}

//function makes div hidden and shift it to another place
var timeId;
function delay_hidden(t)
{
    counter=0;
     timeId=setInterval(function(){
        elem.style.visibility=='hidden'?elem.style.visibility='visible':elem.style.visibility='hidden';
        move_div();
    },t)
}
delay_hidden(t);//calling delay function

setTimeout(function()//set game time
{
    clearInterval(timeId);
    //alert('game over');
    game_over();
},20000);


function game_over()
{
    var div=document.createElement('div');
    div.className="game_over";
    div.innerHTML='game over';
    document.body.appendChild(div);

    // var button=document.createElement('button');
    // button.className="play_again";
    // button.innerHTML='play again';
    // var x=document.getElementsByClassName('game_over');
    // x[0].appendChild(button);
    // button.addEventListener('click',delay_hidden(800));

    var div1=document.getElementById('points');
    div1.style.position='relative';
    div1.style.top=10+"px";
    div1.style.left=50+"px";
    var x=document.getElementsByClassName('game_over');
    x[0].appendChild(div1);

}

