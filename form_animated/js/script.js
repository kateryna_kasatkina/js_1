/**
 * Created by Леля.Полина on 27.04.2017.
 */
$(function(){
   $("form[name=add_event]").submit(function(event){

       if($("input[name=from]").val() == ""){
            $(".date_start img").remove();
            var $arrow = $('<img class="required animated fadeInRightBig" src="img/required.png" alt="required!">');
           $(".date_start td").eq(2).append($arrow);
       }else{
           $(".date_start img").remove();
       }

       event.preventDefault();
   });

   $("input, textarea").focusout(function(){
       if($(this).val() == "") {
           $(this).parents("tr").find("img").remove();
           var $arrow = $('<div class="animated rotateIn"><img class="required animated fadeInRightBig" src="img/required.png" alt="required!"></div>');
           $(this).parents("tr").find("td").last().append($arrow);
       }
   });
});