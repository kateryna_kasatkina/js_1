
var currentAngle = 0;
var radius = 100;
var cX = 0;
var cY = 0;

function drawBox(m) {
    // Draw X/Y coordinates
    var coordX = document.getElementById("coordX");
    cX = coordX.innerHTML = m.pageX;
    cY = coordY.innerHTML = m.pageY;
};

setInterval(function() {
    currentAngle += 0.1;

    // Redraw the box
    var left = cX + (radius * Math.cos(currentAngle));
    var top = cY + (radius * Math.sin(currentAngle));
    var box = document.getElementById("box");
    box.style.left = (left - 50) + "px";
    box.style.top = (top - 25) + "px";
}, 40)