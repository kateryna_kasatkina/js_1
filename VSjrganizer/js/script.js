$(function() {
    var div_more = $('#more');//div for additional events(more than 3)
    var date = new Date();
    var month = date.getMonth();
    var year = date.getFullYear();
    var currentDay = date.getDay();
    var arr_events = [];
    var numberOfDays = 0;//number of dys in current month

    //variables for parts of start dayEvent
    var StartYear = null;
    var StartMonth = null;
    var StartDay = null;
    var FinishYear = null;
    var FinishMonth = null;
    var FinishDay = null;

    var NumberDaysOfEvent = null;//duration of event
    var counterID = null;//for attaching event div
    var wid = null;//width for div of event
    var posNumber = null;//vertical position
    var pos = null;
    var elem = null;//parent td for event div
    var event = null;//object event

    var serial_array = null;//for JSON and localStorge

    var counterTr = null;
    var counterTd = null;
    var counterB = null;
    var str = '';

    //drag and drop for events, dragable only first part of event
    var durationOfEvent = null;//get duration of event from atriibute of div_event
    var oldStartOfEvent = null;//
    var newStartOfevent = null;
    var nameEvent = null;

    var msUp = null;

    //date picker
    $('#sd').datepicker();
    $('#fd').datepicker();
//function gives duration of month
    function durationOfMonth(month)//save number of days in current month into global variable numberOfDays
    {

        switch (month)//get number of days in month
        {
            case 0:
            {
                numberOfDays = 31;
                str = 'Январь'
            }//january
                break;
            case 1:
            {
                (date.getFullYear() % 4 === 0 && date.getFullYear() % 100 !== 0) ? numberOfDays = 29 : numberOfDays = 28;
                str = 'Февраль';
            }//February
                break;
            case 2:
            {
                numberOfDays = 31;
                str = 'Март';
            }//March
                break;
            case 3:
            {
                numberOfDays = 30;
                str = 'Апрель';
            }//April
                break;
            case 4:
            {
                numberOfDays = 31;
                str = 'Май';
            }//May
                break;
            case 5:
            {
                numberOfDays = 30;
                str = 'Июнь';
            }//June
                break;
            case 6:
            {
                numberOfDays = 31;
                str = 'Июль';
            }//July
                break;
            case 7:
            {
                numberOfDays = 31;
                str = 'Август';
            }//Augest
                break;
            case 8:
            {
                numberOfDays = 30;
                str = 'Сеньтябрь';
            }//September
                break;
            case 9:
            {
                numberOfDays = 31;
                str = 'Октябрь';
            }//October
                break;
            case 10:
            {
                numberOfDays = 30;
                str = 'Ноябрь';
            }//November
                break;
            case 11:
            {
                numberOfDays = 31;
                str = 'Декабрь';
            }//December
                break;
        }
        str += ' ';
        str += String(year);
        $('#month').html(str);
        return numberOfDays;
    }

//function drows table width month
    function drowMonth() {
        $('.bodyRow,.bodyTd,.myTable,.tableCaption').remove();

        durationOfMonth(month);

        //get day for 1 of month
        var tmp = new Date();
        tmp.setMonth(month);
        tmp.setFullYear(year);
        tmp.setDate(1);
        var firstDay = tmp.getDay();


        if (firstDay === 0) firstDay = 7;//Sunday=7
        if (currentDay === 0) currentDay = 7;

        //creating table and table-caption
        $('#t').append('<table class="myTable"></table>');
        var MyTable = $('.myTable');
        MyTable.append('<tr class="tableCaption"></tr>');
        $('.tableCaption')
            .append('<td class="captionTr">Пн</td>')
            .append('<td class="captionTr">Вт</td>')
            .append('<td class="captionTr">Ср</td>')
            .append('<td class="captionTr">Чт</td>')
            .append('<td class="captionTr">Пт</td>')
            .append('<td class="captionTr">Сб</td>')
            .append('<td class="captionTr">Вс</td>');

        counterTr = 100;
        counterTd = 1;
        counterB = 1000;
        var is_start = false;
        for (var i = 1; i <= numberOfDays;) {

            if (is_start === false)//drow first week
            {
                MyTable.append('<tr class="bodyRow" id="' + counterTr + '"></tr>');
                is_start = true;
                //part with empty cells
                for (var w = 1; w < firstDay; w++) {
                    $('#' + counterTr).append('<td class="emptyTd"></td>');
                }
                //part with full cells
                for (var f = firstDay; f <= 7; f++) {
                    $('#' + counterTr).append('<td class="bodyTd" id="' + counterTd + '"></td>');
                    $('#' + counterTd)
                        .append('<span class="number">' + counterTd + '</span>')
                        .attr('position', '0')
                        .attr('dayWeek', String(f))
                        .attr('row', String(counterTr))
                        .append('<a class="more" id="' + counterB + '" href="#"><span class="number_more">0</span>more</a>');
                    counterTd++;
                    i++;
                }
                counterTr++;
            }
            MyTable.append('<tr class="bodyRow" id="' + counterTr + '"></tr>');
            for (var j = 1; j <= 7 && i <= numberOfDays; i++ , j++) {
                $('#' + counterTr).append('<td class="bodyTd" id="' + counterTd + '"></td>');
                $('#' + counterTd)
                    .append('<span class="number">' + counterTd + '</span>')
                    .attr('position', '0').attr('dayWeek', String(j))
                    .attr('row', String(counterTr))
                    .append('<a class="more" id="' + counterB + '" href="#"><span class="number_more">0</span>more</a>');
                counterTd++;
            }
            counterTr++;
        }
        $('td').droppable
            ({
                drop: function (event, ui) {
                    var droppableId = $(this).attr("id");
                    var newStartOfevent = Number(droppableId);
                    var newEndOfEvent = newStartOfevent + (Number(durationOfEvent) - 1);
                    //unpack the array
                    var arrayEvents = JSON.parse(localStorage.getItem("ArrayOfEvents"));
                    for (var i = 0; i < arrayEvents.length; i++) {
                        if (arrayEvents[i].name == nameEvent)//&& arrayEvents[i].date_start.getDate == oldStartOfEvent)
                        {
                            DaysEvent(arrayEvents[i]);

                            var tmp_start = new Date();
                            tmp_start.setDate(newStartOfevent);
                            tmp_start.setMonth(month);
                            tmp_start.setFullYear(year);

                            var tmp_finish = new Date();
                            tmp_finish.setDate(newEndOfEvent);
                            tmp_finish.setMonth(month);
                            tmp_finish.setFullYear(year);
                            //alert(tmp_start);
                            arrayEvents[i].date_start = tmp_start;
                            arrayEvents[i].date_finish = tmp_finish;

                        }
                    }
                    serial_array = JSON.stringify(arrayEvents);
                    localStorage.setItem("ArrayOfEvents", serial_array);
                    drowMonth();
                    showEvents();
                }


            });
        //useful for resize
        $('td').mouseup(function () {
                msUP = $(this).attr('id');//put id of td into global variable
            });
        //function more()
        $('.more').click(function () {
            //get id of td
            var d = $(this).parent().attr("id");
            $('#div_more').css("display", "block")
                .html(d + " " + str)
                .append('<button  id="x">x</button>');
            var dd = Number(d);
            getEventsForDay(dd);
        });
        $(document).on('click', '#x', function () {
            $('.more_event_div').remove();
            $('#div_more').css("display", "none");
        });
    }

    drowMonth();
//function heightlight the current day
    function currentTD() {
        var date_current = new Date();
        var day_curr = date_current.getDate();
        //if (date_current.getMonth+1===month)
             $('#' + day_curr).toggleClass('current');
    }
    currentTD();

//button previous
    $('#p').click(function () {
        var prevDate = new Date();
        prevDate.setDate(1);
        if (month === 0) {
            month = 11;
            year -= 1;
        }
        else month -= 1;
        drowMonth();
        showEvents();
    });
//button next
    $('#n').click(function (date) {
        var nextDate = new Date();
        nextDate.setDate(1);
        if (month === 11) {
            month = 0;
            year += 1;
        }
        else month += 1;
        drowMonth();
        showEvents();
    });
//constructor for event
    function EVENT(aDate_start, aDate_finish, aName, aDescription) {
        this.date_start = new Date(aDate_start);
        this.date_finish = new Date(aDate_finish);
        this.name = aName;
        this.description = aDescription;
    }

//show inputs
    $('#circle').click(function () {
        $('#id_wrapper').css("display", "block");
    });
//function devides event which ends in another month
    function devide_event(ev) {
        durationOfMonth(ev.date_start.getMonth());
        var end = numberOfDays;

        var tmp_f = new Date();
        tmp_f.setMonth(ev.date_start.getMonth());
        tmp_f.setFullYear(ev.date_start.getFullYear());
        tmp_f.setDate(end);

        var tmp_s = new Date();
        tmp_s.setMonth(ev.date_finish.getMonth());
        tmp_s.setFullYear(ev.date_finish.getFullYear());
        tmp_s.setDate(1);

        var x1 = new EVENT(ev.date_start, tmp_f, ev.name, ev.description);
        var x2 = new EVENT(tmp_s, ev.date_finish, ev.name, ev.description);
        storage_event(x1);
        storage_event(x2);

    }

//create event for storage
    $('#addEvent').click(function () {
        var s_date = $('#sd').val();
        var f_date = $('#fd').val();
        var ev_name = $('#en').val();
        var ev_description = $('#ds').val();

        var x = new EVENT(s_date, f_date, ev_name, ev_description);
        x.date_start.setHours(x.date_start.getHours() - x.date_start.getTimezoneOffset() / 60);
        x.date_finish.setHours(x.date_finish.getHours() - x.date_finish.getTimezoneOffset() / 60);

        if (x.date_start.getMonth() !== x.date_finish.getMonth()) {
            devide_event(x);
        }
        else {
            storage_event(x);
        }
    });
//save event to local storage
    function storage_event(x) {
        if (localStorage.getItem("ArrayOfEvents") !== null)//not first events
        {
            arr_events = JSON.parse(localStorage.getItem("ArrayOfEvents"));
            arr_events.push(x);
            serial_array = JSON.stringify(arr_events);
            localStorage.setItem("ArrayOfEvents", serial_array);
        }
        else//for first event
        {
            arr_events.push(x);
            serial_array = JSON.stringify(arr_events);
            localStorage.setItem("ArrayOfEvents", serial_array);
        }
    }

//function gets start and finish dates from object and puts them into global variables,counts duration of event
    function DaysEvent(event) {             //for start day
        var start = event.date_start;
        var partsStart = start.split("-");
        StartYear = partsStart[0];//year
        var StartStringMonth = partsStart[1];
        StartStringMonth[0] === '0' ? StartMonth = Number(StartStringMonth[1]) : StartMonth = Number(StartStringMonth[0] + StartStringMonth[1]);
        var StartStringDay = partsStart[2];
        StartStringDay[0] === '0' ? StartDay = StartStringDay[1] : StartDay = StartStringDay[0] + StartStringDay[1];
        //for finish date
        var finish = event.date_finish;
        var partsFinish = finish.split("-");
        FinishYear = partsFinish[0];//year
        var FinishStringMonth = partsFinish[1];
        FinishStringMonth[0] === '0' ? FinishMonth = Number(FinishStringMonth[1]) : FinishMonth = Number(FinishStringMonth[0] + FinishStringMonth[1]);
        var FinishStringDay = partsFinish[2];
        FinishStringDay[0] === '0' ? FinishDay = FinishStringDay[1] : FinishDay = FinishStringDay[0] + FinishStringDay[1];

        //if month the same
        if (StartMonth === FinishMonth)
            NumberDaysOfEvent = (FinishDay - StartDay) + 1;
        //different monthes
        if (StartMonth !== FinishMonth) {
            NumberDaysOfEvent += ((numberOfDays - StartDay) + 1);//days till month ends
            for (var i = StartMonth; i <= FinishMonth; i++)//days in full monthes
            {
                if (month !== 11) {
                    month++;
                    durationOfMonth(month);
                    NumberDaysOfEvent += numberOfDays;
                }
                else {
                    month = 0;
                    durationOfMonth(month);
                    NumberDaysOfEvent += numberOfDays;
                }
            }
            NumberDaysOfEvent += FinishDay;
        }
    }


//if event belong to this month drow it
    function showEvents()
    {
        $('.eventDiv,.eventDiv1,.eventDiv2').remove();
        var mEvents = JSON.parse(localStorage.getItem("ArrayOfEvents"));
        //sort by date_start
        
        var masEvents = mEvents.sort(function (a, b) {
            var dateA = new Date(a.date_start), dateB = new Date(b.date_start)
            return dateA - dateB;
        });

        for (var i = 0; i < masEvents.length; i++)//for each event
        {
            event = masEvents[i];
            DaysEvent(masEvents[i]);//get number of event's days
            if (StartMonth === (month + 1))//select events by month
            {
                drowEvent();
            }
        }
    }

    showEvents();
//attaches div-event to td
    function appendDiv(combination, NumberDaysOfEvent, forAttributeEventStart, is_first_week) {
        switch (combination) {
            case '0100':
                elem.append('<div class="CL eventDiv0 wid_1" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '0200':
                elem.append('<div class="CL eventDiv0 wid_2" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '0200':
                elem.append('<div class="CL eventDiv0 wid_2" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '0300':
                elem.append('<div class="CL eventDiv0 wid_3" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '0400':
                elem.append('<div class="CL eventDiv0 wid_4" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '0500':
                elem.append('<div class="CL eventDiv0 wid_5" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '0600':
                elem.append('<div class="CL eventDiv0 wid_6" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '0700':
                elem.append('<div class="CL eventDiv0 wid_7" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '1100':
                elem.append('<div class="CL eventDiv1 wid_1" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '1200':
                elem.append('<div class="CL eventDiv1 wid_2" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '1300':
                elem.append('<div class="CL eventDiv1 wid_3" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '1400':
                elem.append('<div class="CL eventDiv1 wid_4" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '1500':
                elem.append('<div class="CL eventDiv1 wid_5" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '1600':
                elem.append('<div class="CL eventDiv1 wid_6" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '1700':
                elem.append('<div class="CL eventDiv1 wid_7" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '2100':
                elem.append('<div class="CL eventDiv2 wid_1" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '2200':
                elem.append('<div class="CL eventDiv2 wid_2" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '2300':
                elem.append('<div class="CL eventDiv2 wid_3" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '2400':
                elem.append('<div class="CL eventDiv2 wid_4" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '2500':
                elem.append('<div class="CL eventDiv2 wid_5" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '2600':
                elem.append('<div class="CL eventDiv2 wid_6" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
            case '2700':
                elem.append('<div class="CL eventDiv2 wid_7" DE="' + NumberDaysOfEvent + '"  p="' + forAttributeEventStart + '" dr="' + is_first_week + '">' + event.name + '</div>');
                break;
        }
        ////////////////////////////////////////////////////////////////
        $("[dr='true']").draggable({
            containment: $("#t")
        });
        $("[dr='true']").draggable("option", "containment", $("#t"));
        $("[dr='true']").draggable
            ({
                start: function () {
                    oldStartOfEvent = $(this).attr('p');
                    nameEvent = $(this).html();
                    durationOfEvent = $(this).attr('de');
                },
                drag: function () { },
                stop: function () { }
            });
        ///////////////////////////////////////////////////////////////////


        //$("[dr='true']").resizable({ helper: "ui-resizable-helper" })
        //    .resizable({
        //        grid: [121, 101]
        //    });
    }
//changing vertical position for all effected td
    function changePosition(pos, wid, forStart, elem) {

        var toChange = elem;
        var id = elem.attr('id');
        var ID = Number(id);
        posNumber = Number(pos);
        //alert('pos', posNumber);
        //if(posNumber===3)
        //alert('3 events were attached!');
        //else
            posNumber++;
        for (var i = forStart; i <= (wid / 100); i++) {
            $('#' + ID).attr('position', String(posNumber));
            ID++;
        }

    }

//drow event
    function drowEvent() {
        counterID = Number(StartDay);//id is the number of day start
        elem = $('#' + counterID);//first day of event
        var fStart = elem.attr('dayweek');
        var forStart = Number(fStart);//day of week for first day of event
        var forAttributeEventStart = StartDay;
        var is_first_week = true;
        var temp_counterID = Number(StartDay);
        for (var j = counterID; j < (counterID + NumberDaysOfEvent);)//j=Id
        {
            if (is_first_week === false) forStart = 1;
            wid = 0;
            for (var d = forStart; d <= 7 && j < (counterID + NumberDaysOfEvent); d++ , j++)
            {
                //is_first_week=false;
                wid += 100;//with till week ends
                temp_counterID++;
            }
            pos = elem.attr('position');
            var combination = pos + String(wid);
            //if (is_first_week === true)
           // alert("combination");
            appendDiv(combination, NumberDaysOfEvent, forAttributeEventStart, is_first_week);//append div for element
            is_first_week = false;
            changePosition(pos, wid, forStart, elem);
            var den = elem.attr("dayweek");
            var Nden = Number(den);
            if ((Nden + (wid / 100 - 1)) === 7) {
                var idishnic = temp_counterID;
                elem = $('#' + idishnic);
            }


        }
    }

//function displays events in div_more
    function getEventsForDay(day) {
        var counterEvents = 2000;
        //iteration throught events array     for this month
        var masEvents = JSON.parse(localStorage.getItem("ArrayOfEvents"));
        for (var i = 0; i < masEvents.length; i++)//for each event
        {
            var event = masEvents[i];
            DaysEvent(event);
            if ((StartMonth - 1) === month) {
                //is our day here?
                for (var j = Number(StartDay); j <= Number(FinishDay); j++) {
                    if (day === j)//our day belong to this event
                    {
                        $('#div_more').append('<div class="more_event_div" id="' + counterEvents + '"></div>');
                        $('#' + counterEvents).html(event.name);
                        counterEvents++;
                    }
                }

            }

        }
    }


})

















