<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
<html>
    <body>
        <!--<h2>Group</h2>-->
        <h2>Group<xsl:value-of select="/group/@name"/></h2>
        <table border="1">
        <tr bgcolor="#9acd32">
            <th>Last name</th>
            <th>First name</th>
            <th>PHP</th>
            <th>English</th>
            <th>Math</th>
            <th>ITE</th>
            <th>C</th>
        </tr>
            <xsl:for-each select="group/student">
                <xsl:sort select="last_name"/>
                <tr>
                    <td><xsl:value-of select="last_name"/></td>
                    <td><xsl:value-of select="first_name"/></td>
                    <td><xsl:value-of select="list_subjects/PHP"/></td>
                    <td><xsl:value-of select="list_subjects/english"/></td>
                    <td><xsl:value-of select="list_subjects/math"/></td>
                    <td><xsl:value-of select="list_subjects/ITE"/></td>
                    <td><xsl:value-of select="list_subjects/C"/></td>
                </tr>
            </xsl:for-each>
        </table>
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Last name</th>
                <th>First name</th>
                <th>Overal marck</th>
            </tr>
            <xsl:for-each select="/group/student">
                <xsl:sort select='sum(list_subjects/*) div 4'
                          order="descending"/>
                <tr>
                    <!--<td><xsl:value-of select="name"/></td>-->
                    <td><xsl:value-of select="last_name"/></td>
                    <td><xsl:value-of select="first_name"/></td>
                    <td><xsl:value-of select='sum(list_subjects/*) div 4'/></td>
                </tr>
            </xsl:for-each>
        </table>
    </body>
</html>
    </xsl:template>
</xsl:stylesheet>