$(function () {
    $(".add_event form").submit(function () {
        var dStart = $("tr.date_start input").val();
        var dEnd = $("tr.date_end input").val();
        var name = $("tr.event_name input").val();
        var description = $("tr.description textarea").val();

        if (dStart == "")
        {
            $("<img>").attr("src", "img/required.png").addClass("required")
                .addClass("bounceInRight")
                .appendTo($("tr.date_start td:nth-of-type(3)"));
            event.preventDefault();
        }
        else
            $("tr.date_start td:nth-of-type(3)>img").remove();

        if (dEnd == "")
        {
            
            $("<img>").attr("src", "img/required.png").addClass("required")
                .addClass("bounceInRight")
                .appendTo($("tr.date_end td:nth-of-type(3)"));
            event.preventDefault();
        }
        else
            $("tr.date_end td:nth-of-type(3)>img").remove();

        if (name == "")
        {
            $("<img>").attr("src", "img/required.png").addClass("required")
                .addClass("bounceInRight")
                .appendTo($("tr.event_name td:nth-of-type(3)"));
            event.preventDefault();
        }
        else
            $("tr.event_name td:nth-of-type(3)>img").remove();

        if (description == "")
        {
            $("<img>").attr("src", "img/required.png").addClass("required")
                .addClass("bounceInRight")
                .appendTo($("tr.description td:nth-of-type(3)"));
            event.preventDefault();
        }
        else
            $("tr.description td:nth-of-type(3)>img").remove();
    })
});