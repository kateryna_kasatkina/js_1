/*10 Реализовать игру-головоломку.  Есть поле размером  4х4 заполненное парами изображений.
 При запуске скрипта все изображения открываются на 5 секунд. По истечении времени все
  изображения закрываются и открываются нажатием мыши.
 Первое изображение остается открытым, пока не откроется второе. Если изображения совпадают,
  то они остаются открыты. Если не совпадают – оба закрываются.
 Игра заканчивается, когда открыто все поле. В процессе работы считается кол-во кликов. */
$(function(){
    var number_cell=null;

    var $img_1=$('<img src="img/img (1).jpg">');
    var $img_2=$('<img src="img/img (2).jpg">');
    var $img_3=$('<img src="img/img (3).jpg">');
    var $img_4=$('<img src="img/img (7).jpg">');
    var $img_5=$('<img src="img/img(0).jpg">');
    var $img_6=$('<img src="img/img(4).jpg">');
    var $img_7=$('<img src="img/img(5).jpg">');
    var $img_8=$('<img src="img/img(6).jpg">');
    var $img_9=$('<img src="img/img (1).jpg">');
    var $img_10=$('<img src="img/img (2).jpg">');
    var $img_11=$('<img src="img/img (3).jpg">');
    var $img_12=$('<img src="img/img (7).jpg">');
    var $img_13=$('<img src="img/img(0).jpg">');
    var $img_14=$('<img src="img/img(4).jpg">');
    var $img_15=$('<img src="img/img(5).jpg">');
    var $img_16=$('<img src="img/img(6).jpg">');


    
    var arr_img=[];
    arr_img.push($img_1);
    arr_img.push($img_2);
    arr_img.push($img_3);
    arr_img.push($img_4);
    arr_img.push($img_5);
    arr_img.push($img_6);
    arr_img.push($img_7);
    arr_img.push($img_8);
    arr_img.push($img_9);
    arr_img.push($img_10);
    arr_img.push($img_11);
    arr_img.push($img_12);
    arr_img.push($img_13);
    arr_img.push($img_14);
    arr_img.push($img_15);
    arr_img.push($img_16);
    //alert(arr_img.length);
    var i=1;//counter
    var is_full=false;
    function getRandCell(){
        number_cell=Math.floor((Math.random() * 16) + 1);
    }
  
    function is_Table_full()//chack whether table is full
    {
        for(var number_cell=1;number_cell<16;number_cell++)
        {
            var x=$('#'+number_cell).attr('data-is_free');
            if( x==='yes')//while free
            {
                is_full=false;
            }
            else
                is_full=true;
        }
        
    }
    function place_img_in_random_cell(img)
    {
        var is_placed=false;
        while(is_placed===false)
        {
            getRandCell();//get random cell
            // if cell is free place img
            if( $('#'+number_cell).attr('data-is_free')=='yes')
            {
                $('#'+number_cell)
                    .attr('data-is_free','no')
                    .append(img);
                is_placed=true;
            }
        }
    }
    function fill_Table()
    {
        is_Table_full();
        while(is_full===false)
        {
            is_Table_full();//chack whether table is full
            place_img_in_random_cell(arr_img[i]);
            place_img_in_random_cell(arr_img[i]);
            i++;
       }
   
    }
    fill_Table();
});