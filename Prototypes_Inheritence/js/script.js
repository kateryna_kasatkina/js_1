//'use strict';
//var arr=['red','blue','green'];
//var last=arr[arr.length-1];

//работает только для arr (для arr1 работать не будет!)
/*Object.defineProperty(arr,'last',
    {
        get:function()
        {
            return this[this.length-1]
        }
    });
var last=arr.last;
alert(last);*/


//работает для всех массивов
/*Object.defineProperty(Array.prototype,'last',
    {
        get:function()
        {
            return this[this.length-1];
        }
    })

var arr1=[1,2,3];
var last=arr1.last;
alert(last);*/


//Prototypes
// 'use strict';
// function Cat(aName,aColor)
// {
//     this.name=aName;
//     this.color=aColor;
// };
// Cat.prototype.age=4;
// var fluffy=new Cat('fluffy','white');
// var muffin=new Cat('muffin','brown');
//
//
// fluffy.age=5;
// alert(fluffy.age);//5
// alert(fluffy.__proto__.age);//4
//
//
// alert(Object.keys(fluffy));
// alert(fluffy.hasOwnProperty('age'));


//Changing a Function's Prototype
// 'use strict';
// function Cat(name,color)
// {
//     this.name=name;
//     this.color=color;
// }
// Cat.prototype.age=4;//создали в памяти прототип
// var fluffy=new Cat('fluffy','white');
// var muffin=new Cat('muffin','broun');
// Cat.prototype={age:5};//создали в памяти новый прототип, теперь все коты с возрастом 5
// var x=new Cat('x','y');
//alert(x.age);

//Multiple Levels of Inheritance
/*'use strict';
function Cat(name,color)
{
    this.name=name;
    this.color=color;
};
Cat.prototype.age=4;
var fluffy=new Cat('fluffy','white');
alert(fluffy.__proto__);*/

/*function Animal(voice)
{
    this.voice=voice||"grunt";
};

Animal.prototype.speak=function()
{
    alert(this.voice);
};

function Cat(name,color)
{
    Animal.call(this,"Meow");
    this.name=name;
    this.color=color;
}
Cat.prototype=Object.create(Animal.prototype);
Cat.prototype.constructor=Cat;
var fluffy=new Cat('fluffy','white');
alert(fluffy.__proto__.__proto__);*/


//INHERITANCE CLASSES
'use strict';
class Animal
{
    constructor(voice){this.voice=voice||'grunt';}
    speak(){alert(this.voice);}
};
class Cat extends Animal
{
    constructor(name,color)
    {
        super('Meow');
        this.name=name;
        this.color=color;
    }
};

var fluffy=new Cat('fluffy','white');
alert(Object.keys(fluffy.__proto__.__proto__));//не выводит, но ошибки нет
alert(fluffy.__proto__.__proto__.hasOwnProperty('speak'));





