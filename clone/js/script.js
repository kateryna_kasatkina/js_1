/**
 * Created by admin on 16.03.2017.
 */
var moving_div;
var position = {left:0,top:0};

function select_obj(event){
    var obj = this.querySelector(".select");
    if(obj.style.display == "block"){
        obj.style.display = "none";
        var copy = document.querySelector("#copy_" + this.id);
        copy.remove();
    }else{
        obj.style.display = "block";
        var copy = this.cloneNode(true);
        copy.id="copy_" + this.id;
        document.querySelector(".cart").appendChild(copy);
    }

}
function clear_obj(){
    moving_div = null;
}
function move_obj(event){
    var delta_x = event.clientX - position.left;
    var delta_y = event.clientY - position.top;
    if(moving_div) {
        moving_div.style.left = moving_div.offsetLeft + delta_x + "px";
        moving_div.style.top = moving_div.offsetTop + delta_y + "px";
    }
    position.left = event.clientX;
    position.top = event.clientY;
}


var all_div = document.querySelectorAll(".product")
for(var i =0;i<all_div.length;i++){
    all_div[i].addEventListener("click",select_obj);
}
document.body.addEventListener("mouseup", clear_obj);
document.body.addEventListener("mousemove", move_obj);


