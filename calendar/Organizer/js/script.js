$(function() {
    var date = new Date();
    var month = date.getMonth();
    var year = date.getFullYear();
    var currentDay = date.getDay();
    var arr_events = [];
    var numberOfDays = 0;//number of dys in current month

//variables for parts of start dayEvent
    var StartYear=null;
    var StartMonth=null;
    var StartDay=null;
    var FinishYear=null;
    var FinishMonth=null;
    var FinishDay=null;
    var NumberDaysOfEvent=null;//duration of event
    var counterID=null;//for attaching event div
    var wid=null;//width for div of event
    var posNumber=null;//vertical position
    var pos=null;
    var elem=null;//parent td for event div
    var event=null;//object event

    //date picker
    $('#sd').datepicker();
    $('#fd').datepicker();

    function durationOfMonth(month)//save number of days in current month into global variable numberOfDays
    {
        var str = '';
        switch (month)//get number of days in month
        {
            case 0: {numberOfDays = 31;str = 'Январь'}//january
                break;
            case 1: {(date.getFullYear() % 4 === 0 && date.getFullYear() % 100 !== 0) ? numberOfDays = 29 : numberOfDays = 28;str = 'Февраль';}//February
                break;
            case 2: {numberOfDays = 31;str = 'Март';}//March
                break;
            case 3: {numberOfDays = 30;str = 'Апрель';}//April
                break;
            case 4: {numberOfDays = 31;str = 'Май';}//May
                break;
            case 5: {numberOfDays = 30;str = 'Июнь';}//June
                break;
            case 6: {numberOfDays = 31;str = 'Июль';}//July
                break;
            case 7: {numberOfDays = 31;str = 'Август';}//Augest
                break;
            case 8: {numberOfDays = 30;str = 'Сеньтябрь';}//September
                break;
            case 9: {numberOfDays = 31;str = 'Октябрь';}//October
                break;
            case 10: {numberOfDays = 30;str = 'Ноябрь';}//November
                break;
            case 11: {numberOfDays = 31;str = 'Декабрь';}//December
                break;
        }
        str += ' ';
        str += String(year);
        $('#month').html(str);
        return numberOfDays;
    }

    function drowMonth() {
        $('.bodyRow,.bodyTd,.myTable,.tableCaption').remove();

        durationOfMonth(month);

        //get day for 1 of month
        var tmp = new Date();
        tmp.setMonth(month);
        tmp.setFullYear(year);
        tmp.setDate(1);
        var firstDay = tmp.getDay();


        if (firstDay === 0) firstDay = 7;//Sunday=7
        if (currentDay === 0) currentDay = 7;

        //creating table and table-caption
        $('#t').append('<table class="myTable"></table>');
        var MyTable = $('.myTable');
        MyTable.append('<tr class="tableCaption"></tr>');
        $('.tableCaption')
            .append('<td class="captionTr">Пн</td>')
            .append('<td class="captionTr">Вт</td>')
            .append('<td class="captionTr">Ср</td>')
            .append('<td class="captionTr">Чт</td>')
            .append('<td class="captionTr">Пт</td>')
            .append('<td class="captionTr">Сб</td>')
            .append('<td class="captionTr">Вс</td>');

        var counterTr = 100;
        var counterTd = 1;
        var is_start = false;
        for (var i = 1; i <= numberOfDays;)
        {

            if (is_start === false)//drow first week
            {
                $('.myTable').append('<tr class="bodyRow" id="' + counterTr + '"></tr>');
                is_start = true;
                //part with empty cells
                for (var w = 1; w < firstDay; w++) {
                    $('#' + counterTr).append('<td class="emptyTd"></td>');
                }
                //part with full cells
                for (var f = firstDay; f <= 7; f++) {
                    $('#' + counterTr).append('<td class="bodyTd" id="' + counterTd + '"></td>');
                    $('#' + counterTd).append('<span class="number">' + counterTd + '</span>').attr('position', '0').attr('dayWeek',String(f)).attr('row',String(counterTr));
                    counterTd++;
                    i++;
                }
                counterTr++;
            }
            MyTable.append('<tr class="bodyRow" id="' + counterTr + '"></tr>');
            for (var j = 1; j <= 7 && i <= numberOfDays; i++, j++) {
                $('#' + counterTr).append('<td class="bodyTd" id="' + counterTd + '"></td>');
                $('#' + counterTd).append('<span class="number">' + counterTd + '</span>').attr('position','0').attr('dayWeek',String(j)).attr('row',String(counterTr));
                counterTd++;
            }
            counterTr++;
        }

    }

    drowMonth();

//button previous
    $('#p').click(function () {
        var prevDate = new Date();
        prevDate.setDate(1);
        if (month === 0) {month = 11;year -= 1;}
        else month -= 1;
        drowMonth();
        showEvents();
    });
//button next
    $('#n').click(function (date) {
        var nextDate = new Date();
        nextDate.setDate(1);
        if (month === 11) {month = 0;year += 1;}
        else month += 1;
        drowMonth();
        showEvents();
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //constructor for event
    function EVENT(aDate_start, aDate_finish, aName, aDescription) {
        this.date_start = new Date(aDate_start);
        this.date_finish = new Date(aDate_finish);
        this.name = aName;
        this.description = aDescription;
    }

    /////////////////////////////show inputs/////////////////////////////////////////////////////////
    $('#circle').click(function () {$('#id_wrapper').css("display", "block");});

    //////////////////////////save event to local storage///////////////////////////////////////////////
    $('#addEvent').click(function () {
        var s_date = $('#sd').val();
        var f_date = $('#fd').val();
        var ev_name = $('#en').val();
        var ev_description = $('#ds').val();
        var x = new EVENT(s_date, f_date, ev_name, ev_description);
        x.date_start.setHours(x.date_start.getHours() - x.date_start.getTimezoneOffset() / 60);
        x.date_finish.setHours(x.date_finish.getHours() - x.date_finish.getTimezoneOffset() / 60);

        var serial_array = null;
        if (localStorage.getItem("ArrayOfEvents") !== null)//not first events
        {
            arr_events = JSON.parse(localStorage.getItem("ArrayOfEvents"));
            arr_events.push(x);
            serial_array = JSON.stringify(arr_events);
            localStorage.setItem("ArrayOfEvents", serial_array);
        }
        else//for first event
        {
            arr_events.push(x);
            serial_array = JSON.stringify(arr_events);
            localStorage.setItem("ArrayOfEvents", serial_array);
        }
    });
///////////////////function gets start and finish dates from object and puts them into global variables////////
//////////////////////counts duration of event/////////////////////////////////////////////////////////////////
    function DaysEvent(event)
    {             //for start day
        var start=event.date_start;
        var partsStart = start.split("-");
        StartYear=partsStart[0];//year
        var StartStringMonth=partsStart[1];
        StartStringMonth[0]==='0'?StartMonth=Number(StartStringMonth[1]):StartMonth=Number(StartStringMonth[0]+StartStringMonth[1]);
        var StartStringDay=partsStart[2];
        StartStringDay[0]==='0'?StartDay=StartStringDay[1]:StartDay=StartStringDay[0]+StartStringDay[1];
                  //for finish date
        var finish=event.date_finish;
        var partsFinish=finish.split("-");
        FinishYear=partsFinish[0];//year
        var FinishStringMonth=partsFinish[1];
        FinishStringMonth[0]==='0'?FinishMonth=Number(FinishStringMonth[1]):FinishMonth=Number(FinishStringMonth[0]+FinishStringMonth[1]);
        var FinishStringDay=partsFinish[2];
        FinishStringDay[0]==='0'? FinishDay=FinishStringDay[1]:FinishDay=FinishStringDay[0]+FinishStringDay[1];

        //if month the same
        if(StartMonth===FinishMonth)
            NumberDaysOfEvent=(FinishDay-StartDay)+1;
        //different monthes
        if(StartMonth!==FinishMonth)
        {
            NumberDaysOfEvent+=((numberOfDays-StartDay)+1);//days till month ends
            for(var i=StartMonth;i<=FinishMonth;i++)//days in full monthes
            {
                if(month!==11)
                {
                    month++;
                    durationOfMonth(month);
                    NumberDaysOfEvent+=numberOfDays;
                }
                else
                {
                    month=0;
                    durationOfMonth(month);
                    NumberDaysOfEvent+=numberOfDays;
                }
            }
            NumberDaysOfEvent+=FinishDay;
        }
    }
    function showEvents()//if event belong to this month drow it
    {
        $('.eventDiv,.eventDiv1,.eventDiv2').remove();
        var masEvents = JSON.parse(localStorage.getItem("ArrayOfEvents"));
        for (var i = 0; i < masEvents.length; i++)//for each event
        {
            event=masEvents[i];
            DaysEvent(masEvents[i]);//get number of event's days
            if (StartMonth === (month + 1))
            {
                drowEvent();
            }//select events by month
        }
    }
    showEvents();
    ///////////////////////////////attaches div to td///////////////////////////////////////////////////////
    function appendDiv(combination)
    {
        switch (combination)
        {
            case '0100':elem.append('<div class="eventDiv0 wid_1">'+ event.name + '</div>');break;
            case '0200':elem.append('<div class="eventDiv0 wid_2">'+ event.name + '</div>');break;
            case '0300':elem.append('<div class="eventDiv0 wid_3">'+ event.name + '</div>');break;
            case '0400':elem.append('<div class="eventDiv0 wid_4">'+ event.name + '</div>');break;
            case '0500':elem.append('<div class="eventDiv0 wid_5">'+ event.name + '</div>');break;
            case '0600':elem.append('<div class="eventDiv0 wid_6">'+ event.name + '</div>');break;
            case '0700':elem.append('<div class="eventDiv0 wid_7">'+ event.name + '</div>');break;
            case '1100':elem.append('<div class="eventDiv1 wid_1">'+ event.name + '</div>');break;
            case '1200':elem.append('<div class="eventDiv1 wid_2">'+ event.name + '</div>');break;
            case '1300':elem.append('<div class="eventDiv1 wid_3">'+ event.name + '</div>');break;
            case '1400':elem.append('<div class="eventDiv1 wid_4">'+ event.name + '</div>');break;
            case '1500':elem.append('<div class="eventDiv1 wid_5">'+ event.name + '</div>');break;
            case '1600':elem.append('<div class="eventDiv1 wid_6">'+ event.name + '</div>');break;
            case '1700':elem.append('<div class="eventDiv1 wid_7">'+ event.name + '</div>');break;
            case '2100':elem.append('<div class="eventDiv2 wid_1">'+ event.name + '</div>');break;
            case '2200':elem.append('<div class="eventDiv2 wid_2">'+ event.name + '</div>');break;
            case '2300':elem.append('<div class="eventDiv2 wid_3">'+ event.name + '</div>');break;
            case '2400':elem.append('<div class="eventDiv2 wid_4">'+ event.name + '</div>');break;
            case '2500':elem.append('<div class="eventDiv2 wid_5">'+ event.name + '</div>');break;
            case '2600':elem.append('<div class="eventDiv2 wid_6">'+ event.name + '</div>');break;
            case '2700':elem.append('<div class="eventDiv2 wid_7">'+ event.name + '</div>');break;
        }

        elem.sortable({placeholder:"placeholder"});
        //$(".eventDiv0,.eventDiv1,.eventDiv2").resizable();
    }


    ////////////////////////////////////////changing vertical position for all effected td///////////////////////////////////////////////////////
    function changePosition(pos,wid,forStart,elem)
    {

        var toChange=elem;
        var id=elem.attr('id');
        var ID=Number(id);
        posNumber=Number(pos);
        if(posNumber===3)
            alert('3 events were attached!')
        else posNumber++;
        for(var i=forStart;i<=(wid/100);i++)
        {
            $('#'+ID).attr('position',String(posNumber));
            ID++;
        }

    }
    /////////////////////////////////////////drow event///////////////////////////////////////////////////////////////////////////
function drowEvent()
{
        counterID=Number(StartDay);//id is the number of day start
        elem = $('#' + counterID);//first day of event
        var fStart=elem.attr('dayweek');
        var forStart=Number(fStart);
        var is_first_week=true;
        for(var j=counterID;j<(counterID+NumberDaysOfEvent);)//j=Id
        {
            if(is_first_week===false) forStart=1;
                wid=0;
            for(var d=forStart;d<=7&&j<(counterID+NumberDaysOfEvent);d++,j++)
            {
                is_first_week=false;
                wid+=100;//with till week ends
            }
            pos = elem.attr('position');
            var combination=pos+String(wid);
            appendDiv(combination);//append div
            changePosition(pos,wid,forStart,elem);
            var den=elem.attr("dayweek");
            var Nden=Number(den);
            if((Nden+(wid/100-1))===7)
            {
                var idishnic=counterID+(wid/100-1)+1;
                elem = $('#' +idishnic);
            }

        }
}
});



















