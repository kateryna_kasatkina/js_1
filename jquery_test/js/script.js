$(function(){
    var date = new Date();
    var month=date.getMonth();
    var year=date.getFullYear();
    var currentDay=date.getDay();
    var activeTdId=null;
    var arr_events=[];

    //date picker
    $('#sd').datepicker();
    $('#fd').datepicker();

function drowMonth()
{
    $('.bodyRow,.bodyTd,.myTable,.tableCaption').remove();
    var numberOfDays=0;
    var str='';
    switch(month)//get number of days in month
    {
        case 0:{numberOfDays=31;
            str='Январь'}//january
            break;
        case 1:{(date.getFullYear()%4===0&&date.getFullYear()%100!==0)? numberOfDays=29:numberOfDays=28;//February
            str='Февраль';}
            break;
        case 2:{numberOfDays=31;
        str='Март';}//March
            break;
        case 3:{numberOfDays=30;
        str='Апрель';}//April
            break;
        case 4:{numberOfDays=31;
        str='Май';}//May
            break;
        case 5:{numberOfDays=30;
        str='Июнь';}//June
            break;
        case 6:{numberOfDays=31;
        str='Июль';}//July
            break;
        case 7:{numberOfDays=31;
        str='Август';}//Augest
            break;
        case 8:{numberOfDays=30;
        str='Сеньтябрь';}//September
            break;
        case 9:{numberOfDays=31;
        str='Октябрь';}//October
            break;
        case 10:{numberOfDays=30;
        str='Ноябрь';}//November
            break;
        case 11:{numberOfDays=31;
        str='Декабрь';}//December
            break;
    }

    $('#month').html(str);

    //get day for 1 of month
    var tmp= new Date();
    tmp.setMonth(month);
    tmp.setFullYear(year);
    tmp.setDate(1);
    var firstDay=tmp.getDay();


    if(firstDay===0)
      firstDay=7;//Sunday=7

    //var currentDay=date.getDay();//get number of current day in a week
    if(currentDay===0)
        currentDay=7;

    //creating table and table-caption
    $('#t').append('<table class="myTable"></table>');
    var MyTable=$('.myTable');
    MyTable.append('<tr class="tableCaption"></tr>')
        .append('<td class="captionTr">Пн</td>')
        .append('<td class="captionTr">Вт</td>')
        .append('<td class="captionTr">Ср</td>')
        .append('<td class="captionTr">Чт</td>')
        .append('<td class="captionTr">Пт</td>')
        .append('<td class="captionTr">Сб</td>')
        .append('<td class="captionTr">Вс</td>');

    var counterTr=100;
    //id of td is date
    var counterTd=1;

    var is_start=false;
    for(var i=1;i<=numberOfDays;)
    {

        if(is_start===false)//drow first week
        {
            $('.myTable').append('<tr class="bodyRow" id="'+counterTr+'"></tr>');
            is_start=true;
            //part with empty cells
            for(var w=1;w<firstDay;w++)
            {
                $('#'+counterTr).append('<td class="emptyTd"></td>');
            }
            //part with full cells
            for(var f=firstDay;f<=7;f++)
            {
               var ID=String(year)+'/'+String(month)+'/'+String(i);
                $('#'+counterTr).append('<td class="bodyTd" id="'+counterTd+'"></td>');
                $('#'+counterTd).append('<span class="number">'+counterTd+'</span>').attr('position',0+'day');
                counterTd++;i++;
            }
            counterTr++;
        }
        MyTable.append('<tr class="bodyRow" id="'+counterTr+'"></tr>');
        for(var j=1;j<=7&&i<=numberOfDays;i++,j++)
        {
                $('#'+counterTr).append('<td class="bodyTd" id="'+counterTd+'"></td>');
                $('#'+counterTd).append('<span class="number">'+counterTd+'</span>').attr('position',0+'day');
                counterTd++;
        }
        counterTr++;
    }


    $('td.bodyTd').click(function(){
        $(this).toggleClass('active');
    });
}
    drowMonth();

//button previous
    $('#p').click(function(date){
        //create new date
        var prevDate=new Date();
        prevDate.setDate(1);
        if(month===0)
        {
            month=11;
            year-=1;
        }
        else
        {
            month-=1;
        }
        drowMonth();
    })
//button next
    $('#n').click(function(date){
        //create new date
        var nextDate=new Date();
        nextDate.setDate(1);
        if(month===11)
        {
            month=0;
            year+=1;
        }
        else
        {
            month+=1;
        }
        drowMonth();
    })

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //constructor for event
    function EVENT(aDate_start,aDate_finish,aName,aDescription)
    {
        this.date_start=new Date(aDate_start);
        this.date_finish=new Date(aDate_finish);
        this.name=aName;
        this.description=aDescription;

    }
    ///show inputs
    $('#circle').click(function(){$('#id_wrapper').css("display","block");});

    //save to local storage
  $('#addEvent').click(function(){
       var s_date=$('#sd').val();
       var f_date=$('#fd').val();
       var ev_name=$('#en').val();
       var ev_description=$('#ds').val();
       var x=new EVENT(s_date,f_date,ev_name,ev_description);

       //change position of days
      var day_start=s_date.getDate();
      var day_finish=f_date.getDate();
      for(var i=day_start;i<=day_finish;i++)
      {
          //get position
          $('#'+i).position=''
      }

      //drow the event


      var serial_array=null;
       if(localStorage.getItem("ArrayOfEvents")!==null)//not first events
       {
           arr_events=JSON.parse(localStorage.getItem("ArrayOfEvents"));
           arr_events.push(x);
           serial_array=JSON.stringify(arr_events);
           localStorage.setItem("ArrayOfEvents",serial_array);
       }
       else//for first event
       {
           alert('first');
           arr_events.push(x);
           serial_array=JSON.stringify(arr_events);
           localStorage.setItem("ArrayOfEvents",serial_array);
       }
});


 function clear_form(){
     $('#sd').val('');
     $('#fd').val('');
     $('#en').val('');
     $('#ds').val('');
 }
    clear_form();































});